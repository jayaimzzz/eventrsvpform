const express = require("express");
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/rsvp');

const port = 3000;
const publicPath = ('public/');

const app = express();
// app.use(express.json());
app.set('views', './views');
app.set('view engine', 'pug');
app.use(express.urlencoded());

const db = mongoose.connection;

db.on('error', console.error.bind(console, 'database connetion error:'));
db.once('open', function(){
    console.log("we're connected");
    const RSVPSchema = new mongoose.Schema({
        name: String,
        email: String,
        attending: Boolean,
        numberOfGuests: Number
    })

    const Response = mongoose.model('Response', RSVPSchema);

    app.post('/rsvp', (req, res) => {
        let body = new Response({
            name: req.body.name,
            email: req.body.email,
            attending: req.body.attending,
            numberOfGuests: req.body.numberOfGuests
        })
        body.save();
        console.log(body);
        res.render('submitted');
    })

    app.get('/guestlist', async function(req, res) {
        let attending = await Response.find({'attending': 'true'}, (err, array) => array)
        let notAttending = await Response.find({'attending': 'false'}, (err, array) => array)
        attending = attending.map(attendee => attendee.name)
        notAttending = notAttending.map(attendee => attendee.name)
        res.render('guestlist',{
            attending: attending,
            notAttending: notAttending
        })
    });

    app.listen(port);


})

app.get('/', (req, res) => {
    res.render('index');
})


